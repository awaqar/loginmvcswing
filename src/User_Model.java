/**
 * Created by macintosh on 29/08/2017.
 */
public class User_Model {
    private String user;
    private String password;
    private boolean is_valid;

    public User_Model(String user, String password) {
        this.user = user;
        this.password = password;
        is_valid = false;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public boolean authenticate(String user,String password){
        if(this.user.equalsIgnoreCase(user) && this.password.equalsIgnoreCase(password)) {
            is_valid = true;
        }
           return is_valid;
    }
}
