import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;

/**
 * Created by macintosh on 29/08/2017.
 */
public class Users_Directory {
    private ArrayList<User_Model> users;

    public Users_Directory() {
        users = new ArrayList<User_Model>();
    }

    public void add(String username, String password){
        users.add(new User_Model(username,password));
    }

    public void add(User_Model user_model){
        users.add(user_model);
    }

    public User_Model getUser(int i){
        return users.get(i);
    }

    public void showUsers(){
        for(int i=0; i<users.size();i++){
            System.out.println(users.get(i));
        }
    }
}
