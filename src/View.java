import javax.swing.*;
import java.awt.*;

/**
 * Created by macintosh on 29/08/2017.
 */
public class View {
    private JFrame frame;
    private JLabel namelabel;
    private JLabel passlabel;
    private  JLabel result;
    private JButton button;
    private JTextField namefield;
    private JTextField passfield;

    public View(){
        frame = new JFrame("View");
        frame.setLayout(new GridLayout(3,3));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,300);

        namefield = new JTextField();
        passfield = new JTextField();
        namelabel = new JLabel("Name: ");
        passlabel = new JLabel("password: ");
        result = new JLabel("Not logged in");
        button = new JButton("Login");

        frame.add(namelabel);
        frame.add(namefield);
        frame.add(passlabel);
        frame.add(passfield);
        frame.add(result);
        frame.add(button);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

    public JButton getButton(){return this.button;}

    public String getNamefieldText(){return this.namefield.getText();}

    public  String getPassFieldText(){
        return this.passfield.getText();
    }

    public void setResultLabel(String text){
        this.result.setText(text);
    }
}
