import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by macintosh on 29/08/2017.
 */
public class Controller {

    private User_Model model;
    private View view;
    private ActionListener actionListener;

    public Controller(User_Model model,View view){
        this.model = model;
        this.view = view;
    }


    public void perform(){
        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                linkViewandModel();
            }
        };

        view.getButton().addActionListener(actionListener);
    }

    public void linkViewandModel(){
        boolean result = model.authenticate(view.getNamefieldText(),view.getPassFieldText());
        view.setResultLabel(Boolean.toString(result));

    }
}
