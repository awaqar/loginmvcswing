import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;

import java.lang.reflect.Modifier;

/**
 * Created by macintosh on 29/08/2017.
 */
public class Main {
    public static void main(String [] args){
        User_Model model = new User_Model("Azzum","ismail");
        View view = new View();
        Controller controller = new Controller(model,view);
        controller.perform();
    }
}
